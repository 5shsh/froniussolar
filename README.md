# froniussolar

Docker image accessing Fronius API for transforming Solar panels information into MQTT messages.

## Run

```
docker run -d \
    --name froniussolar \
    --restart=unless-stopped \
    -v /config/env:/env \ \
    homesmarthome/froniussolar2mqtt:0.1.0
```
Expects ```MQTT_HOST``` and ```FRONIUS_HOST```value in env file.