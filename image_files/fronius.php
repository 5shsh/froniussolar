<?php

define('TOPIC_PREFIX', '/froniussolar');
$MQTT_HOST = getenv('MQTT_HOST');
$FRONIUS_HOST = getenv('FRONIUS_HOST');

$c = new Mosquitto\Client;

$powerFlowRealtimeData = function() use ($FRONIUS_HOST) {
    $url = "http://$FRONIUS_HOST/solar_api/v1/GetPowerFlowRealtimeData.fcgi";
    
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0); 
    curl_setopt($ch, CURLOPT_TIMEOUT, 4);
    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        throw new Exception(curl_error($ch));
    }
    curl_close($ch);

    return $result;
};

$status = function($data) use ($c) {
    $data = json_decode($data, true);

    $payload['self_consumption']['val'] = $data['Body']['Data']['Site']['rel_SelfConsumption'];
    $payload['self_consumption']['unit'] = '%';

    $payload['production']['val'] = $data['Body']['Data']['Site']['P_PV'];
    $payload['production']['unit'] = 'W';
    $payload['consumption']['val'] = $data['Body']['Data']['Site']['P_Load'];
    $payload['consumption']['unit'] = 'W';
    $payload['consumption_grid']['val'] = $data['Body']['Data']['Site']['P_Grid'];
    $payload['consumption_grid']['unit'] = 'W';

    $payload['energy_day']['val'] = $data['Body']['Data']['Site']['E_Day'];
    $payload['energy_day']['unit'] = 'Wh';
    $payload['energy_year']['val'] = $data['Body']['Data']['Site']['E_Year'];;
    $payload['energy_year']['unit'] = 'Wh';
    $payload['energy_total']['val'] = $data['Body']['Data']['Site']['E_Total'];;
    $payload['energy_total']['unit'] = 'Wh';

    $c->publish(TOPIC_PREFIX . '/status', json_encode($payload), 2, false);
};

$c->onConnect(function() use ($c) {
    $c->subscribe(TOPIC_PREFIX . '/get', 2);
});

$c->onMessage(function($message) use ($powerFlowRealtimeData, $status) {
    printf("Got a message on topic %s\n", $message->topic);
    try {
        $payload = $powerFlowRealtimeData();
        var_dump($payload);
        $status($payload);
    }
    catch (\Exception $e) {
        echo "Exception\n";
        $status('{"Body": {"Data": {"Site": {"rel_SelfConsumption": -1, "P_PV": -1, "P_Load": 0, "P_Grid": 0, "E_Day": -1, "E_Year": -1, "E_Total": -1 }}}}');
    }    
});

echo "Connecting to $MQTT_HOST\n";
$c->connect($MQTT_HOST);
$c->loopForever();
echo "Finished\n";